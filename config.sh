yum -y update
yum -y install git-core zlib zlib-devel gcc-c++ patch readline readline-devel libyaml-devel libffi-devel openssl-devel make bzip2 autoconf automake libtool bison curl sqlite-devel
cd
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bash_profile
echo 'eval "$(rbenv init -)"' >> ~/.bash_profile
source ~/.bash_profile
git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bash_profile
source ~/.bash_profile
rbenv install -l
rbenv install 2.3.1
rbenv global 2.3.1
echo "gem: --no-ri --no-rdoc" > ~/.gemrc
gem install bundler
gem install rails
rbenv rehash
yum -y install epel-release
yum -y install nodejs
yum -y install mysql
#sudo systemctl start mysqld
#mysql_secure_installation
